@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Pesquisar Cliente</h2>
        </div>
    </div>
</div>

<form action="" method="POST">
    @csrf
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <input type="text" id="identificacao" name="identificacao" class="form-control" placeholder="Digite seu Nome, RG ou CPF">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Pesquisar</button>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="result-pesquisa"></div>
    </div>
</div>
<script>
    
var _token = $('meta[name="_token"]').attr('content');

$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': _token

    }

});     
    

$(document).on("submit", "form", function(e){
    
    e.preventDefault();
    var identificacao = $("input#identificacao").val();
    
    $.ajax({
      url: "{{ route('find.client') }}",
      type: "POST",
     
      data:{identificacao:identificacao} ,
      success: function (response) {
        
        var fraude = 'Não'
        var responseParse = JSON.parse(response);
        responseParse.fraude;
        
        if(responseParse.fraude) {
           
            fraude = 'Sim'
        }
          
          $(".result-pesquisa").html('<p class="font-weight-bold">'+responseParse.nome+', fraude: '+fraude+'.</p>');
      }
    });
    
});    
</script>
@endsection

