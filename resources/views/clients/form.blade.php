@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Cadastrar Novo Cliente</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href=""> Back</a>
        </div>
    </div>
</div>

<form action="{{ route('clients.create') }}" method="POST">
    @csrf

     <div class="row">
         <input type="hidden" name="user_id" value="1">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nome:</strong>
                <input type="text" name="nome" class="form-control" placeholder="Digite seu Nome">
            </div>
        </div>
         
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Data de Nascimento:</strong>
                <input maxlength="10" onkeypress="mascaraData( this, event )"  type="text" name="data_aniversario" class="form-control" placeholder="Data de Nascimento (ex.:07/02/1988)">
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>RG:</strong>
                <input type="text" name="rg" class="form-control" placeholder="Digite seu RG(Somente Números)">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>CPF:</strong>
                <input maxlength="11" type="text" name="cpf" class="form-control" placeholder="Digite seu CPF(Somente Números)">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>CEP:</strong>
                <input id="cep" maxlength="8" type="tel" name="cep" class="form-control" placeholder="Digite o CEP(Somente Números)">
            </div>
        </div>
        <div class="load-cep">
            
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
               {!! csrf_field() !!}
                    <div class="form-group">
                        <div class="file-loading">
                            <input id="images" type="file" name="images" multiple class="file" data-overwrite-initial="false" data-min-file-count="2">
                        </div>
                    </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
<script>

$(".load-cep").hide();

var _token = $('meta[name="_token"]').attr('content');

$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': _token

    }

});     

$("#cep").blur(function() {

    var cep = $(this).val().replace(/[^0-9]/, '');
    console.log(cep);
    if(cep.length==8){
            $.ajax({
                url: "{{route('clients.address')}}",
                data:{cep:cep},
                type: "POST",
                //dataType: 'json',
                success : function(json){
                    $(".load-cep").show();
                    $(".load-cep").html(json);
                }
            });

    }					
});	
    
function mascaraData( campo, e )
{
	var kC = (document.all) ? event.keyCode : e.keyCode;
	var data = campo.value;
	
	if( kC!=8 && kC!=46 )
	{
		if( data.length==2 )
		{
			campo.value = data += '/';
		}
		else if( data.length==5 )
		{
			campo.value = data += '/';
		}
		else
			campo.value = data;
	}
}
</script>
@endsection