<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('clients/getInfoCep', ['as' => 'clients.address', 'uses' => 'App\Http\Controllers\Client\ClientController@getInfoCep']);

Route::get('clients/form', 'App\Http\Controllers\Client\ClientController@formClient');
Route::post('clients/storeClient', ['as' => 'clients.create', 'uses' => 'App\Http\Controllers\Client\ClientController@storeClient']);

Route::get('clients/search', 'App\Http\Controllers\Client\ClientController@findClients');
Route::post('clients/findBy', ['as' => 'find.client', 'uses' => 'App\Http\Controllers\Client\ClientController@findBy']);