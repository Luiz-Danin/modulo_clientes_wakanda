<?php
declare (strict_types=1);

namespace App\Models\Client;

use App\Models\Address\Address;
use App\Models\Photos\ClientPhoto;
use App\Models\User;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    
    protected $table = 'clients';
        
    protected $fillable = [
      'user_id', 'nome', 'data_aniversario','rg','cpf'
    ];
    
    public $timestamps = false;
    
    public function address()
    {
        return $this->hasMany(Address::class);
    }
    
    public function photo() 
    {
        return $this->hasMany(ClientPhoto::class);
    }
    
    public function user() 
    {
        return $this->belongsTo(User::class);
    }
}
