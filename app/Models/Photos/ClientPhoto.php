<?php

namespace App\Models\Photos;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Client\Client;

class ClientPhoto extends Model
{
    use HasFactory;
    
    protected $table = 'client_photos';
        
    protected $fillable = [
      'client_id','photo','is_thumb'
    ];
    
    public function client() 
    {
        return $this->belongsTo(Client::class);
    }
}
