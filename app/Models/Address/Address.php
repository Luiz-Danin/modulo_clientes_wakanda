<?php

namespace App\Models\Address;

use App\Models\Client\Client;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;
    
    protected $table = 'addresses';
        
    protected $fillable = [
      'client_id','logradouro',
      'numero','bairro',
      'municipio','UF','is_active'
    ];
    
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
