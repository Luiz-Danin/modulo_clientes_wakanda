<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
/**
 * Description of AbstractRepository
 *
 * @author luiz
 */
abstract class AbstractRepository implements RepositoryInterface
{
    protected Model $model;
    
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function create(array $data): array
    {
        return $this->model::create($data)->toArray();
    }

    public function delete(int $id): bool
    {
        return $this->model::destroy($id) ? true : false;
    }

    public function editBy(string $param, array $data): bool
    {
        $result = $this->model::find($param)->update($data);
        
        return $result ? true : false;
    }

    /**
     * @param int $limit
     * @param array $orderBy
     * @return array
     */
    public function findAll(int $limit = 10, array $orderBy = []): array
    {
        $results = $this->model::query();

        foreach ($orderBy as $key => $value) {
            if (strstr($key, '-')) {
                $key = substr($key, 1);
            }

            $results->orderBy($key, $value);
        }

        return $results->paginate($limit)
            ->appends([
                'order_by' => implode(',', array_keys($orderBy)),
                'limit' => $limit
            ])
            ->toArray();
    }

    public function findOneBy(int $id): array
    {
        return $this->model::findOrFail($id)->toArray();
    }

}