<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare(strict_types=1);
namespace App\Repositories\Client;

use App\Repositories\AbstractRepository;

/**
 * Description of ClientRepository
 *
 * @author luiz
 */
class ClientRepository extends AbstractRepository 
{
    
    public function findByNomeRgCpf(string $param): array
    {
        $query = $this->model::query();
        
         if (is_numeric($param)) {
             
            $srt_param = strlen($param);

            if ($srt_param==11) {
                 $clients = $query->where('cpf', $param)
                    ->get();
            } else {
                $clients = $query->where('rg', $param)->get();
            }
            
         } else {
             
             $clients = $query->where('nome', $param)->get();
         }

        return $clients->toArray();
    }
    
    public function storeClient(array $data)
    {
        return $this->model::create($data);
    }
    
}