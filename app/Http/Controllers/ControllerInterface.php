<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);
namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
/**
 *
 * @author luiz
 */
interface ControllerInterface 
{
    public function create(Request $request): JsonResponse;
    
    public function findAll(Request $request): JsonResponse;
    
    public function findOneBy(Request $request, int $id): JsonResponse;
    
    public function editBy(Request $request, string $param): JsonResponse;
    
    public function delete(Request $request, int $id): JsonResponse;
}