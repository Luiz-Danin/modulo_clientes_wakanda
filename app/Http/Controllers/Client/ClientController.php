<?php
namespace App\Http\Controllers\Client;

use App\Http\Controllers\AbstractController;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\ClientRequest;
use Illuminate\Http\Request;

use App\Services\Client\ClientService;
use Exception;

class ClientController extends AbstractController
{
    protected array $estados = [
                                'AC','AL','AM','BA','CE','DF',
                                'ES','GO','MA','MS','MT','MG',
                                'PA','PB','PR','PE','PI','RJ',
                                'RN','RS','RO','SC','SP','SE',
                                'TO'
        ];


    public function __construct(ClientService $service) 
    {
        parent::__construct($service);
    }
    
    public function getInfoCep(Request $request)
    {
        $cep = $request->get('cep');
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_HEADER, false);
        $url = 'https://viacep.com.br/ws/'.$cep.'/json/';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_SSLVERSION,3); 
        $result = curl_exec($ch);
        
        curl_close($ch);
        $result_decode = json_decode($result, true);
       
        $infoCep =  '<div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Endereço:</strong>
                    <input id="endereco" value='.$result_decode['logradouro'].' type="text" name="endereco" class="form-control" placeholder="Digite o seu Endereço">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Número:</strong>
                    <input id="numero" type="text" name="numero" class="form-control" placeholder="Digite a numeração">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Bairro:</strong>
                    <input id="bairro" value='.$result_decode['bairro'].' type="text" name="bairro" class="form-control" placeholder="Digite o seu Bairro">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Municipio:</strong>
                    <input id="municipio" value='.$result_decode['localidade'].' type="text" name="municipio" class="form-control" placeholder="Digite o seu Municipio">
                </div>
            </div>';
        
        $infoCep .= '<div class="col-xs-12 col-sm-12 col-md-12">
                     <div class="form-group">
                     <label for="uf">Selecione um Estado:</label>
                     <select class="form-control" id="uf" name="uf">
                     <option value="">Selecione</option>';
        
        for($i=0; $i<count($this->estados); $i++)
        {
            $infoCep .= '<option value="'.$this->estados[$i].'">'.$this->estados[$i].'</option>';
            if($this->estados[$i]==$result_decode['uf'])
            {
                $infoCep .= '<option selected value="'.$this->estados[$i].'">'.$this->estados[$i].'</option>';
            }
        }
        
        $infoCep .= ' </div>
                     </div>';
        
        echo $infoCep;
    }


    public function formClient()
    {
        return view('clients.form');
    }
    
    public function storeClient(Request $request) 
    {
        
        $data = $request->all();
        $images = $request->file('images');
        
        try {
            
            $data_aniversario = implode('-', array_reverse(explode('/', substr($data['data_aniversario'], 0, 10)))).substr($data['data_aniversario'], 10);
            $data['data_aniversario'] = $data_aniversario;

            $result = $this->service->storeClient($data);
            dd($result);
        
            if($images) {

                $imagesUploaded = [];

                foreach ($images as $image) {

                    $path = $image->store('images', 'public');
                    $imagesUploaded[] = ['photo' => $path, 'is_thumb' => false];
                }

                $result->photo()->createMany($imagesUploaded);
            }
        
        return view('clients.form');
        
        } catch (Exception $e) {
            
        }
    }


    public function findClients()
    {
        return view('clients.formPesquisa');
    }
    
    public function findBy(Request $request)
    {
        try{
            $identificacao= $request->get('identificacao');
            
            $result = $this->service->findByNomeRgCpf($identificacao);
            $isFraud = $this->checkFraud($result[0]);
            
            $result[0]['fraude'] = $isFraud;
            
            $response = $this->successResponse($result);
        } catch (Exception $e){
            $response = $this->erroResponse($e);
        }
        
        //return response()->json($response, $response['status_code']);
        echo json_encode($result[0], JSON_FORCE_OBJECT);
    }
    
    
    private function checkFraud(array $data): bool 
    {
        $fraude = true;
        
        $data_aniversario = strtotime($data['data_aniversario']);
        $ano_data_aniversario = date("Y",$data_aniversario);
        $ano_data_aniversario_int = (int)$ano_data_aniversario;
        
        if($ano_data_aniversario_int<=1950) {
            
            $cpf = substr($data['cpf'], 0, 1); 
            
            if( ($cpf==0) || ($cpf==1) || ($cpf==2) || ($cpf==3)) {
                $fraude = false;
            }
            
        }
        elseif ( ( $ano_data_aniversario_int > 1950 ) && ($ano_data_aniversario_int<=2000) ) {
            
            $cpf = substr($data['cpf'], 0, 1); 
            
            if( ($cpf==4) || ($cpf==5) || ($cpf==6) ) {
                
                $fraude = false;
            }
        }
        else {
            
            $cpf = substr($data['cpf'], 0, 1); 
            
            if( ($cpf==7) || ($cpf==8) || ($cpf==9) ) {
                
                $fraude = true;
            }
        }
        
        return $fraude;
    }
}