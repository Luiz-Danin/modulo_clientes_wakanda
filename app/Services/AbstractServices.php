<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Services;

use App\Services\ServiceInterface;
use App\Repositories\RepositoryInterface;
/**
 * Description of AbstractServices
 *
 * @author luiz
 */
abstract class AbstractServices implements ServiceInterface
{
    protected RepositoryInterface $repository;
    
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
    
    public function create(array $data): array 
    {
       return $this->repository->create($data);
    }
    
    public function findAll(int $limit = 10, array $orderBy = []): array 
    {
        return $this->repository->findAll($limit, $orderBy);
    }
    
    public function findOneBy(int $id): array 
    {
        return $this->repository->findOneBy($id);
    }
    
    public function editBy(string $param, array $data): bool 
    {
        return $this->repository->editBy($param, $data);
    }
    
    public function delete(int $id): bool
    {
        return $this->repository->delete($id);
    }
    
}