<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare (strict_types=1);
namespace App\Services\Client;

use App\Services\AbstractServices;
/**
 * Description of ClientService
 *
 * @author luiz
 */
class ClientService extends AbstractServices
{
    
    
    public function findByNomeRgCpf(string $param):array
    {
        return $this->repository->findByNomeRgCpf($param);
    }
    
    public function storeClient(array $data) 
    {
       return $this->repository->storeClient($data);
    }
    
}