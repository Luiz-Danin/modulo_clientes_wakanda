<?php
declare (strict_types = 1);
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Client\ClientService;
use App\Repositories\Client\ClientRepository;
use App\Models\Client\Client;
/**
 * Description of ClientServiceProvider
 *
 * @author luiz
 */
class ClientServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(ClientService::class, function($app){
            return new ClientService(new ClientRepository( new Client() ) );
        });
    }
}